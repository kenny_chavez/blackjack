public class User{
    public int id;
    public String name;
    public String nick;
    public int wins = 0;
    public int loses = 0;
    public int games = 0;
    public String type;

    public User(int id, String name, String nick, String type){
        this.id = id;
        this.name = name;
        this.nick = nick;
        this.type = type;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getNick(){
        return this.nick;
    }

    public void setNick(String nick){
        this.nick = nick;
    }

    public int getWins(){
        return this.wins;
    }

    public void setWins(int wins){
        this.wins = wins;
    }

    public int getLoses(){
        return this.loses;
    }

    public void setLoses(int loses){
        this.loses = loses;
    }

    public int getGames(){
        return this.games;
    }

    public void setGames(int games){
        this.games = games;
    }

    public String getType(){
        return this.type;
    }

    public void setType(String type){
        this.type = type;
    }
}