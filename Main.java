public class Main{
    public static void main(String args[]){
        GameController game = new GameController();
        int option = 0;
        game.showWelcome();
        
        while(option != 3){
            option = game.showMenu();

            switch (option) {
                case 1:
                    game.start();
                    break;
            
                default:
                    System.out.println("\nOpcion invalida.");
                    break;
            }
        }
    }
}