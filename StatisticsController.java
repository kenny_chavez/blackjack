import java.util.ArrayList;

public class StatisticsController{
    public void setGames(ArrayList<User> user){
        int games = user.get(0).getGames() + 1;
        user.get(0).setGames(games);
    }

    public void setResult(ArrayList<User> user, int resultGame){
        int result = resultGame;
        if(result == 0){
            result = user.get(0).getLoses();
            user.get(0).setLoses(result);
        }else{
            result = user.get(0).getWins();
            user.get(0).setWins(result);
        }
    }
}