public class Card{
    public int id;
    public String figure;
    public String value;

    public Card(String figure, String value){
        this.figure = figure;
        this.value = value;
    }

    public String getFigure(){
        return this.figure;
    }

    public void setFigure(String figure){
        this.figure = figure;
    }

    public String getValue(){
        return this.value;
    }

    public void setValue(String value){
        this.value = value;
    }
}