import java.util.ArrayList;
import java.util.Scanner;
import java.lang.Math;

public class GameController{
    public String name, nick, type = "player", playerOption = "s";
    public String[] values = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Q", "J", "K" };
    public String[] symbols = { "corazon", "trebol", "espadas", "diamantes" };
    Scanner line = new Scanner(System.in);
    ArrayList<Card> cardsPlayer = new ArrayList<>();
    ArrayList<Card> cardsCroupier = new ArrayList<>();
    ArrayList<User> users = new ArrayList<>();

    public void clearScreen() {  
        System.out.print("\033[H\033[2J");  
        System.out.flush();  
    }
    
    public void showWelcome(){
        System.out.print("Nombre: ");
        name = line.nextLine();
        System.out.print("Nick: ");
        nick = line.nextLine();
        User player = new User(1, name, nick, type);
        users.add(player);

        this.clearScreen();
        System.out.println("\nBIENVENIDO A BLACKJACK, " + nick +"!");
    }
    
    public int showMenu(){
        int option = 0;
        String nick = users.get(0).getNick();
        String name = users.get(0).getName();

        System.out.println("Jugador: " + name);
        System.out.println("Nick: " + nick);
        System.out.println("\n\nMENU");
        System.out.println("1. Jugar");
        System.out.println("2. Ver estadísticas");
        System.out.println("3. Salir\n\n");
        System.out.print("Option: ");
        option = Integer.parseInt(line.nextLine());
        this.clearScreen();

        return option;
    }

    public String getRandomValue(String[] data){
        int random = (int) Math.floor(Math.random()*data.length);

        return data[random];
    }

    public Card createNewCard(){
        return new Card(getRandomValue(this.symbols), getRandomValue(this.values));
    }

    public int showAllCards(ArrayList<Card> cards){
        int total = 0;
        for(int i = 0; i < cards.size(); i++){
            int noCard = i + 1;
            System.out.println("Carta " + "[" + noCard + "]: " + cards.get(i).getValue() + " " + cards.get(i).getFigure());

            if(cards.get(i).getValue() == "A" || cards.get(i).getValue() == "Q" || cards.get(i).getValue() == "J" || cards.get(i).getValue() == "K"){
                if(total > 10 && cards.get(i).getValue() == "A"){
                    total += 1;
                }else if(total < 11 && cards.get(i).getValue() == "A"){
                    total += 11;
                }else{
                    total += 10;
                }
            }else{
                total += Integer.parseInt(cards.get(i).getValue());
            }
        }

        System.out.println("-----------------");
        System.out.println("TOTAL: " + total);
        System.out.println("-----------------");

        return total;
    }

    public boolean validate(int total){
        boolean isvalid = false;

        if(total <= 21){
            isvalid = true;
        }

        return isvalid;
    }

    public void start(){
        int totalPlayer = 0, totalCroupier = 0,attepmtsMax = 5, attepmtsMin = 2, random = 0;
        String any = "";
        Scanner line2 = new Scanner(System.in);
        
        while (!this.playerOption.equals("n")) {
            this.clearScreen();

            System.out.println("-----------------");
            System.out.println("Turno del Jugador ");
            System.out.println("-----------------");

            cardsPlayer.add(this.createNewCard());
            totalPlayer = showAllCards(this.cardsPlayer);
            
            if(!validate(totalPlayer)){
                System.out.println("\n\nHas perdido.");
                break;
            }

            System.out.print("Desea tomar otra carta? (s/n): ");
            this.playerOption = line2.nextLine().toLowerCase().trim();
        }

        if(validate(totalPlayer)){
            System.out.println("-----------------");
            System.out.println("Turno del Crupier");
            System.out.println("-----------------");
            random = (int) (Math.random() * ((attepmtsMax - attepmtsMin) + 1)) + attepmtsMin;

            for(int a = 0; a < random; a++){
                cardsCroupier.add(createNewCard());
            }
            
            totalCroupier = this.showAllCards(cardsCroupier);

            if(!validate(totalCroupier)){
                System.out.println("\n>> Has ganado");
            }else if(validate(totalCroupier) && totalCroupier < totalPlayer){
                System.out.println("\n>> Has ganado");
            }else if(validate(totalCroupier) && totalCroupier > totalPlayer){
                System.out.println("\n>> Has perdido");
            }
        }

        cardsPlayer.clear();
        cardsCroupier.clear();
        this.playerOption = "";

        System.out.println("\n\nPresiona cualquier tecla para continuar");
        any = line2.next();

        if(any != null){
            this.clearScreen();
            return;
        }
    }
}